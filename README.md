# Build instructions

Cannot make it on CI, so run it on local with `podman` and `make` as requirements.

Clone https://github.com/abrt/faf with the latest changes as we requiere some still unreleased changes.

**WE STILL HAVE MANY REMAINING ISSUES, BUT NOT BLOCKING: <https://github.com/abrt/faf/issues?q=is%3Aissue+label%3ACERN+>**

```
# You will need to login to CERN's registry to push images
sudo podman login gitlab-registry.cern.ch
# Build FAF
sudo podman build -f container/Dockerfile ./
sudo podman image tag <image_id_from_previous_step> gitlab-registry.cern.ch/linuxsupport/faf:latest
sudo podman image push gitlab-registry.cern.ch/linuxsupport/faf:latest
# Build DB image (should be replaced at some point by our DBoD instance)
sudo podman build -f container/Dockerfile_db ./
sudo podman image tag <image_id_from_previous_step> gitlab-registry.cern.ch/linuxsupport/faf:db
sudo podman image push gitlab-registry.cern.ch/linuxsupport/faf:db
```

# Deployment 

* CI is prepared to deploy FAF and all its requirements as a Helm deployment, just press run manual job and you are done.

* Prepare it internally by adding releases and repos to sync (~600GB)

```
# Add our releases and remove fedora ones
faf releaseadd -o centos --opsys-release 7 --status ACTIVE
faf releaseadd -o centos --opsys-release 8 --status ACTIVE
faf releasedel -o fedora --opsys-release 30
faf releasedel -o fedora --opsys-release 31
faf releasedel -o fedora --opsys-release 32
faf releasedel -o fedora --opsys-release 33
faf releasedel -o fedora --opsys-release rawhide


faf repoadd cc7_centosplus-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/centosplus-testing/x86_64/
faf repoadd cc7_centosplus-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/centosplus-testing/Debug/x86_64/
faf repoadd cc7_centosplus dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/centosplus/x86_64/

faf repoadd cc7_cern-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cern-testing/x86_64/
faf repoadd cc7_cern-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cern-testing/Debug/x86_64/
faf repoadd cc7_cern dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/
faf repoadd cc7_cern_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cern/Debug/x86_64/

# DO NOT ADD THEM FOR NOW: https://github.com/abrt/faf/issues/927
#faf repoadd cc7_cernonly-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cernonly-testing/x86_64/
#faf repoadd cc7_cernonly-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cernonly-testing/Debug/x86_64/
#faf repoadd cc7_cernonly dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/
#faf repoadd cc7_cernonly_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cernonly/Debug/x86_64/

faf repoadd cc7_cloud-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cloud-testing/x86_64/openstack-queens/ \
  http://linuxsoft.cern.ch/cern/centos/7/cloud-testing/x86_64/openstack-rocky/ \
  http://linuxsoft.cern.ch/cern/centos/7/cloud-testing/x86_64/openstack-stein/ \
  http://linuxsoft.cern.ch/cern/centos/7/cloud-testing/x86_64/openstack-train/
faf repoadd cc7_cloud dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cloud/x86_64/openstack-queens/ \
  http://linuxsoft.cern.ch/cern/centos/7/cloud/x86_64/openstack-rocky/ \
  http://linuxsoft.cern.ch/cern/centos/7/cloud/x86_64/openstack-stein/ \
  http://linuxsoft.cern.ch/cern/centos/7/cloud/x86_64/openstack-train/
faf repoadd cc7_cloud_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/cloud/Debug/x86_64/

#faf repoadd cc7_cr-testing dnf --nogpgcheck
#faf repoadd cc7_cr dnf --nogpgcheck

faf repoadd cc7_extras-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/extras-testing/x86_64/
faf repoadd cc7_extras dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/extras/x86_64/

faf repoadd cc7_fasttrack-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/fasttrack-testing/x86_64/
faf repoadd ccc7_fasttrack dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/fasttrack/x86_64/

faf repoadd cc7_os dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/

faf repoadd ccc7_rhcommon-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rhcommon-testing/x86_64/
faf repoadd ccc7_rhcommon-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rhcommon-testing/Debug/
faf repoadd cc7_rhcommon dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rhcommon/x86_64/
faf repoadd cc7_rhcommon_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rhcommon/Debug/x86_64/

faf repoadd cc7_rt-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rt-testing/x86_64/
faf repoadd cc7_rt-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rt-testing/Debug/
faf repoadd cc7_rt dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rt/x86_64/
faf repoadd cc7_rt_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/rt/Debug/x86_64/

faf repoadd cc7_sclo-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/sclo-testing/x86_64/
#faf repoadd cc7_sclo_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/sclo/Debug/x86_64/
#faf repoadd cc7_sclo dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/sclo/x86_64/

faf repoadd cc7_storage-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/storage-testing/x86_64/
faf repoadd cc7_storage-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/storage-testing/Debug/x86_64/
faf repoadd cc7_storage dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/storage/x86_64/
faf repoadd cc7_storage_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/storage/Debug/x86_64/

faf repoadd cc7_updates-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/updates-testing/x86_64/
faf repoadd cc7_updates-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/updates-testing/Debug/x86_64/
faf repoadd cc7_updates dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/
faf repoadd cc7_updates_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/updates/Debug/x86_64/

faf repoadd cc7_virt-testing dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/virt-testing/x86_64/
faf repoadd cc7_virt-testing_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/virt-testing/Debug/x86_64/
faf repoadd cc7_virt dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/virt/x86_64/
faf repoadd cc7_virt_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/7/virt/Debug/x86_64/



faf repoadd cc8_appstream dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/AppStream/x86_64/os/
faf repoadd cc8_baseos dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/BaseOS/x86_64/os/
faf repoadd cc8_cern dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/
faf repoadd cc8_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/Debug/x86_64/
faf repoadd cc8_devel dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/Devel/x86_64/os/
faf repoadd cc8_highavailability dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/HighAvailability/x86_64/os/
faf repoadd cc8_powertools dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/PowerTools/x86_64/os/
faf repoadd cc8_centosplus dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/centosplus/x86_64/os/Packages/
faf repoadd cc8_cloud dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/cloud/x86_64/openstack-train/ \
  http://linuxsoft.cern.ch/cern/centos/8/cloud/x86_64/openstack-ussuri/
faf repoadd cc8_cloud_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/cloud/Debug/x86_64/
faf repoadd cc8_cr dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/cr/x86_64/os/
faf repoadd cc8_extras dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/extras/x86_64/os/
faf repoadd cc8_fasttrack dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/fasttrack/x86_64/os/
faf repoadd cc8_messaging dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/messaging/x86_64/qpid-proton/ \
  http://linuxsoft.cern.ch/cern/centos/8/messaging/x86_64/rabbitmq-38/
faf repoadd cc8_messaging_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/messaging/Debug/x86_64/
faf repoadd cc8_openafs dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/openafs/x86_64/
faf repoadd cc8_sclo dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/sclo/x86_64/
faf repoadd cc8_sclo_debug dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/sclo/Debug/x86_64/
faf repoadd cc8_virt dnf --nogpgcheck http://linuxsoft.cern.ch/cern/centos/8/virt/x86_64/advanced-virtualization/ \
  http://linuxsoft.cern.ch/cern/centos/8/virt/x86_64/ovirt-44/



faf repoadd c7_atomic dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/atomic/x86_64/
faf repoadd c7_centosplus dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/centosplus/x86_64/
faf repoadd c7_cloud dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/cloud/x86_64/openstack-queens/ \
  https://linuxsoft.cern.ch/centos/7/cloud/x86_64/openstack-rocky/ \
  https://linuxsoft.cern.ch/centos/7/cloud/x86_64/openstack-stein/ \
  https://linuxsoft.cern.ch/centos/7/cloud/x86_64/openstack-train/
faf repoadd c7_configmanagement dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/configmanagement/x86_64/ansible26/ \
  https://linuxsoft.cern.ch/centos/7/configmanagement/x86_64/ansible-27/ \
  https://linuxsoft.cern.ch/centos/7/configmanagement/x86_64/ansible-28/ \
  https://linuxsoft.cern.ch/centos/7/configmanagement/x86_64/ansible-29/
faf repoadd c7_cr dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/cr/x86_64/
faf repoadd c7_dotnet dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/dotnet/x86_64/
faf repoadd c7_extras dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/extras/x86_64/
faf repoadd c7_fasttrack dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/fasttrack/x86_64/
faf repoadd c7_nfv dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/nfv/x86_64/
faf repoadd c7_opstools dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/opstools/x86_64/
faf repoadd c7_os dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/os/x86_64/
faf repoadd c7_paas dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin13/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin14/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin15/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin310/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin311/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin36/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin37/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin38/ \
  https://linuxsoft.cern.ch/centos/7/paas/x86_64/openshift-origin39/
faf repoadd c7_rt dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/rt/x86_64/
faf repoadd c7_sclo dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/sclo/x86_64/rh/ \
  https://linuxsoft.cern.ch/centos/7/sclo/x86_64/sclo/
faf repoadd c7_storage dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/storage/x86_64/ceph-jewel/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/ceph-luminous/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/ceph-nautilus/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/gluster-3.12/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/gluster-4.0/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/gluster-4.1/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/gluster-5/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/gluster-6/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/gluster-7/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/nfs-ganesha-28/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/nfs-ganesha-30/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/nfsganesha-28/ \
  https://linuxsoft.cern.ch/centos/7/storage/x86_64/nfsganesha-30/
faf repoadd c7_updates dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/updates/x86_64/
faf repoadd c7_virt dnf --nogpgcheck https://linuxsoft.cern.ch/centos/7/virt/x86_64/azure-kernel/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/azure/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/kubernetes110/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/kubernetes19/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/kvm-common/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/libvirt-latest/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/ovirt-4.2/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/ovirt-4.3/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/ovirt-4.4/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/ovirt-common/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/xen-410/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/xen-412/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/xen-448/ \
  https://linuxsoft.cern.ch/centos/7/virt/x86_64/xen-common/



faf repoadd c8_appstream dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/AppStream/x86_64/os/
faf repoadd c8_baseos dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/BaseOS/x86_64/os/
faf repoadd c8_devel dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/Devel/x86_64/os/
faf repoadd c8_highavailability dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/HighAvailability/x86_64/os/
faf repoadd c8_powertools dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/PowerTools/x86_64/os/
faf repoadd c8_centosplus dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/centosplus/x86_64/os/
faf repoadd c8_cloud dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/cloud/x86_64/openstack-train/ \
  https://linuxsoft.cern.ch/centos/8/cloud/x86_64/openstack-ussuri/
faf repoadd c8_configmanagement dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/configmanagement/x86_64/ansible-29/
faf repoadd c8_cr dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/cr/x86_64/os/
faf repoadd c8_extras dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/extras/x86_64/os/
faf repoadd c8_fasttrack dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/fasttrack/x86_64/os/
faf repoadd c8_messaging dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/messaging/x86_64/qpid-proton/ \
  https://linuxsoft.cern.ch/centos/8/messaging/x86_64/rabbitmq-38/
faf repoadd c8_opstools dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/opstools/x86_64/collectd-5/
faf repoadd c8_storage dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/storage/x86_64/ceph-nautilus/ \
  https://linuxsoft.cern.ch/centos/8/storage/x86_64/ceph-octopus/ \
  https://linuxsoft.cern.ch/centos/8/storage/x86_64/ceph-octopus/ \
  https://linuxsoft.cern.ch/centos/8/storage/x86_64/gluster-6/ \
  https://linuxsoft.cern.ch/centos/8/storage/x86_64/gluster-7/ \
  https://linuxsoft.cern.ch/centos/8/storage/x86_64/nfsganesha-3/ \
  https://linuxsoft.cern.ch/centos/8/storage/x86_64/nfsganesha-28/
faf repoadd c8_virt dnf --nogpgcheck https://linuxsoft.cern.ch/centos/8/virt/x86_64/advanced-virtualization/ \
  https://linuxsoft.cern.ch/centos/8/virt/x86_64/ovirt-44/

faf repoadd epel7 dnf --nogpgcheck http://linuxsoft.cern.ch/epel/7/x86_64/
faf repoadd epel7_debug dnf --nogpgcheck http://linuxsoft.cern.ch/epel/7/x86_64/debug/

faf repoadd epel8 dnf --nogpgcheck http://linuxsoft.cern.ch/epel/8/Everything/x86_64/
faf repoadd epel8_debug dnf --nogpgcheck http://linuxsoft.cern.ch/epel/8/Everything/x86_64/debug/


# Assign repos to their releases
faf repolist | grep c7 | while read line ; do faf repoassign $line "CentOS 7" x86_64 ; done
faf repolist | grep epel7 | while read line ; do faf repoassign $line "CentOS 7" x86_64 ; done
faf repolist | grep c8 | while read line ; do faf repoassign $line "CentOS 8" x86_64 ; done
faf repolist | grep epel8 | while read line ; do faf repoassign $line "CentOS 8" x86_64 ; done
```

## Users

[FAF requires to use a FAS account, period.](https://github.com/abrt/faf/issues/904)

We can control who is an admin after logging in for the first time (we should be checking email, as a stranger can create an account with your username):

```
oc rsh <podname>
psql
faf=# UPDATE users SET admin = 't' WHERE username = 'djuarezg';
```

We might add a cronjob for doing this automatically according to egroup membership.

## Celery tasks

This part is really important as FAF depends on recurrent actions to run. There are different ways this can be actually done:
* We could use Openshift Cronjobs for this scheduling of actions, but this would spawn new pods, which we should be careful about in order not to run out of processing and memory quota.
* Using [`uwsgi`](https://uwsgi-docs.readthedocs.io/en/latest/Configuration.html) [as in the upstream Dockerfile](https://github.com/abrt/faf/blob/master/container/Dockerfile#L59-60).
  * This will make recurrent tasks log on `/var/log/faf/uwsgi_logs`
* **Use Celery Tasks**. For this to work we need to run a parallel Redis instance. This can be done through the web UI: https://abrtanalytics.web.cern.ch/faf/celery_tasks/

There are some tasks that are really needed for this to fully work:
* `faf reposync`: we need to reposync all rpms so we can retrace reports.
* `faf addcompathashes`: we need to create compatible hashes of reports so they can be attached to pre-existing CentOS Mantis Bugtracker bugs.
* `faf find-crashfn`: we need to find crashing functions for reports so they are listed on report details and tables.
* `faf find-components`: This creates a component per package, retrieved from repos.
* `faf match-unknown-packages`: This cleans up a table where all packages not known when receiving reports are stored. If we receive a report with a package not yet repo-synced we need to cleanup this table with this action.
* `faf retrace`: given a report, it will look for related packages, both binary and debuginfo and try to create a backtrace for the report so it can help detect issues associated to reports.
* `faf assign-release-to-builds CentOS 7 x86_64` & `faf assign-release-to-builds CentOS 8 x86_64`: Not really sure but this can help if a reposync left packages in inconsistent state (for example when reposync broke due to an uncatched exception leaving some unflushed changes to DB)
* `faf attach-centos-bugs`: This task links existing reports/problems to CentOS Mantis Bugtracker and marks them as solved if it is the case.

### Configuring recurrent taks

As an admin user (check [the users section](#users)), go to <https://abrtanalytics.web.cern.ch/faf/celery_tasks/>, where you will need to configure the following tasks:

* `faf reposync`: 0 0 * * *, at midnight. This is the one that takes the most (specially for the first time)
* `faf find-components`: 0 4 * * *, at 4:00
* `faf addcompathashes`: 0 * * * *, every hour.
* `faf find-crashfn`: 30 * * * *, every hour at hh:30.
* `faf match-unknown-packages`: 0 6 * * *, at 6:00.
* `faf retrace`: 0 8 * * *, at 8:00. This one takes more or less depending on the number reports and if they can be retraced.
* `faf attach-centos-bugs`: 0 5 * * *, at 5:00. This one takes ~10min

Please note FAF [already runs recurrent actions via `uwgsi`](https://github.com/abrt/faf/blob/master/container/Dockerfile#L59-60). This means that there is no need to add actions for `save-reports` or `create-problems`, but we could add them so they run whenever we want.

## Notes for DBoD

Whenever DBoD solves this issue: [RQF1592999](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1592999)

Preparing the instance requires the following steps:

* Connect with the credentials given on [RQF1586285](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF1586285)

```
psql -h dbod-abrtanalytics.cern.ch -p 6603 -U admin
```

* Run the following commands
```
CREATE USER faf WITH PASSWORD '<thepassword>';
ALTER USER faf VALID UNTIL 'infinity';
GRANT faf TO admin
CREATE DATABASE faf OWNER faf;
GRANT ALL PRIVILEGES ON DATABASE faf TO faf;
```
* Connect to the `faf` database

```
psql -h dbod-abrtanalytics.cern.ch -p 6603 -U admin -d faf
```

* Enable `semver` extension (only doable by DBoD admins):
```
CREATE EXTENSION semver
```

So as of today DB is still running in OKD. Please find the DBoD credentials on CI/CD variables (can't use Teigi as there is no hostgroup for this service): `ADMIN_DB_PWD`, `FAF_DB_PWD`

## CI/CD

This project's CI requires setting up a service account on its namespace:

```
oc login openshift.cern.ch
oc project abrtanalytics
oc create sa openshift-operations
oc policy add-role-to-user admin system:serviceaccount:abrtanalytics:openshift-operations
oc sa get-token openshift-operations
# Note the token on https://gitlab.cern.ch/linuxsupport/faf/-/settings/ci_cd -> ABRTANALYTICS_PROD_OPENSHIFT. REMEMBER TO SELECT MASK VARIABLE!!!
```

## TODO

* Cronjob for synchronising admin users according to egroup membership, otherwise this requires manual action as stated on the [Users section](#users)
* Cronjob for noticing when a new repo has been added so we can reposync it as well
  * This can be tricky as determining the appropriate repo URL is not trivial, also we might not be interested into reposync-ing all repos.

